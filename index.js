'use strict';

    const Microservice  = require('swagger-microservice'),
          swagger       = require('./petstore.json'),
          Express       = require('express'),
          app           = new Express();

    let serverPort = 8081;
    let server = new Microservice();

    server.initialize(swagger, app, function(err) {
        server.startServer(serverPort, function(err) {
            if(err) {
                console.log("Error", err);
            }
            else {
                console.log("server started");
            }
        });
    });