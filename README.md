# Swagger-Microservice In-Memory Datastore Server Example 

Uses the [Swagger-Microservice](https://gitlab.com/apitheory/swagger-microservice) project to drive a REST API that stores data to an in-memory datastore.  

Run as Node app
----------------------

The easiest way to run the application is by installing the files into a directory and running npm start.  This will fire up index.js which will start a server listening on port 8081.  Try POSTing {"name": "rover"} to /v1/pets and then attempt to GET /v1/pets.  An array with 1 item - Rover - should be returned.

```
npm start
``` 

Run as Docker Container
----------------------
In the root directory is a Dockerfile that will build a container for the application.  

From the directory, run the following command (you can name the image anything you want).
```
$ docker build -t apitheory/swagger-microservice-example-inmemory:v1 .
```  

Once it is built, double check that the build is available by executing the command (the apitheory/swagger-microservice-example-inmemory should appear in the list):
```
$ docker images
```  

You can then run the Docker build:
```
$ docker run -p 8081:8081 -d apitheory/swagger-microservice-example-inmemory:v1
```  
Before attempting to retrieve an endpoint from the server, determine the docker IP address:
```
$ docker-machine ip default
```
Copy the IP address that is displayed and then try to hit the URL http://{dockeripdefault}:8081/v1/pets. This request should return an empty array.